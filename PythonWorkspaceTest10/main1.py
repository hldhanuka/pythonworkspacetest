class Account:
    def __init__(self) -> None:
        self.balance = 0

    def get_balance(self) -> float :
        return self.balance

account = Account()

account.balance = 1000.00

print(account.get_balance())
print(account.balance)