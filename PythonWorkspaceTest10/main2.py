class Account:
    def __init__(self) -> None:
        self._balance = 0

    def get_balance(self) -> float :
        return self._balance

account = Account()

account._balance = 1000.00

print(account.get_balance())
print(account._balance)