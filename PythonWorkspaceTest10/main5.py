class Account:
    def __init__(self) -> None:
        self.__balance = 0

    @property
    def balance(self) -> float :
        return self.__balance

    @balance.setter
    def balance(self, balance : float) -> None :
        self.__balance = balance

account = Account()

account.balance = 1000.00

print(account.balance)