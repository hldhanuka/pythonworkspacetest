class Account:
    def __init__(self) -> None:
        self.__balance = 0

    def get_balance(self) -> float :
        return self.__balance

    def set_balance(self, balance : float) -> None :
        self.__balance = balance

account = Account()

account.__balance = 1000.00

print(account.get_balance())
print(account.__balance)

account.set_balance(1500.00)

print(account.get_balance())
print(account.__balance)