class MyClass:
    def __init__(self, name : str):
        self.name = name

    def say_hello(self) -> None:
        print(f"Hello {self.name}")

my_object = MyClass('Dhanuka')

my_object.say_hello()