class Song:
    def __init__(self, name, year):
        self.name = name
        self.year = year

class Album:
    def __init__(self, name):
        self.name = name
        self.songs = []

    def add_song(self, song : Song):
        self.songs.append(song)

class Artist:
    def __init__(self, name):
        self.name = name
        self.aldums = []
        self.songs = []

    def add_album(self, aldum : Album):
        self.aldums.append(aldum)

    def add_song(self, song : Song):
        self.songs.append(song)

class Playlist:
    def __init__(self, name):
        self.name = name
        self.songs = []

    def add_song(self, song : Song):
        self.songs.append(song)

artist1 = Artist('Dhauka')

album1 = Album('Aldum Dhanuka')

song1 = Song('S1 - Dhauka', '2022')
song2 = Song('S2 - Dhauka', '2022')

album1.add_song(song1)
album1.add_song(song2)

artist1.add_song(song1)

playlist1 = Playlist("Dhanuka PlayList")

for song in album1.songs:
    playlist1.add_song(song)

print(playlist1.songs)
print(artist1.songs)