class Robot:
    # initialise class variable
    counter = 0

    def __init__(self, name):
        self.name = name

    def introduce(self):
        Robot.counter += 1

        print(f"Hi! I'm {self.name}")

    def number_active_robots():
        if Robot.counter == 0:
            print(f"There are {Robot.counter} active robots")
        else:
            print(f"There are still  {Robot.counter} active robots")

    def remove(self):
        Robot.counter -= 1

        print(f"Removing {self.name}")

robot1 = Robot('R1')
robot1.introduce()

Robot.number_active_robots()

robot2 = Robot('R2')
robot2.introduce()

Robot.number_active_robots()

robot1.remove()

robot3 = Robot('R3')
robot3.introduce()

Robot.number_active_robots()

robot2.remove()
robot3.remove()

Robot.number_active_robots()