import datetime

# function for validating user input
def validate(nic : str) -> bool:
    nic_length = len(nic)

    if nic_length != 12 :
        return False
    else:
        return nic.isdigit()

# function for identifying born year
def get_born_year(nic : str) -> int :
    year = nic[0:4]

    return int(year)

# get born date
def get_born_date(nic : str) -> datetime.date:
    born_day = int(nic[4:7])

    year = get_born_year(nic)

    jan_first = datetime.date(year, 1, 1)

    born_date = jan_first + datetime.timedelta(days=born_day - 1)

    return born_date

# get the nic number as a user input
nic_number = input("Please enter your NIC number : ")

# validate user input
validate_nic = validate(nic_number)

if not validate_nic :
    print("Invalid NIC! Please check again the number")

    exit(0)

# Identify the born year
year = get_born_year(nic_number)

# Calculate the exact born date
born_date = get_born_date(nic=nic_number)

# Prit the output
iso_date = born_date.isoformat()

print(f"Your birthday : {iso_date}")

day = born_date.strftime('%A')
print(f"Born day : {day}")