import datetime

# Function for validating old nic as user input


def validate(old_nic: str, * V: str) -> bool:
    old_nic_length = len(old_nic)
    if old_nic_length != 9:
        return False
    else:
        is_digit = old_nic.isdigit()
        return is_digit

# Function fir old identify born year


def get_born_year(old_nic: str) -> int:
    year = old_nic[0: 4]
    return int(year)

# get born date for old nic


def get_born_date(old_nic: str) -> datetime.date:
    born_day = int(old_nic[4:2])
    year = get_born_year(old_nic)
    aprial_two = datetime.date(year, 1, 1)
    born_date = aprial_two + datetime.timedelta(days=born_day-2)

# To identify Gender


def get_born_day(old_nic: str) -> datetime.date:
    born_day = int(old_nic[4: 2])
    year = get_born_year(old_nic)
    return born_day


# Get old nic number as a user input
old_nic_number = input("Please Enter Your Valid Old NIC Number   : ")

# Validate user input for old nic
valid_old_nic = validate(old_nic_number)

# Idenify Born year from old nic
year = get_born_year(old_nic_number)

# calculate exact born date from old nic
born_date = get_born_date(old_nic=old_nic_number)

# Print the out put
iso_date = born_date.isoformat()
print(f"Your Birthday :{iso_date}")
day = born_date.strftime('%A')
print(f"Born day : {day}")
