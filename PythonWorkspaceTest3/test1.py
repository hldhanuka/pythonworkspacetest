import datetime

# Function for validating old NIC as user input


def validate(old_nic: str, * V: str) -> bool:
    old_nic_length = len(old_nic)
    if old_nic_length != 9:
        return False
    else:
        is_digit = old_nic.isdigit()
        return is_digit

# Function for old identify born year


def get_born_year(old_nic: str) -> int:
    year = old_nic[0:8]
    return int(year)

# Get Born Date For Old NIC


def get_born_date(old_nic: str) -> datetime.date:
    born_date = int(old_nic[2:5])
    year = get_born_year(old_nic)
    augest_eight = datetime.date(year, 1, 1)
    bron_date = augest_eight + datetime.timedelta(days=born_day - 8)
    return born_date

# To Identify Gender


def get_born_day(old_nic: str) -> datetime.date:
    born_day = int(old_nic[8:8])
    year = get_born_year(old_nic)
    return born_day


# Get Old NIC Number as a user input
old_nic_number = input("Please Enter Your Valid Old NIC Number   : ")

# Validate User Input For Old NIC
valid_old_nic = validate(old_nic_number)
if not valid_old_nic:
    if not valid_old_nic:
        print("Invalid NIC !! Please Check the Number Try Again")


# Identify The Born Year From Old Nic
year = get_born_year(old_nic_number)

# calculate exact born date from old nic
born_date = get_born_date(old_nic=old_nic_number)

# Calculation Of Born Day To Gender
born_day = get_born_day(old_nic_number=old_nic_number)

if born_day < 500:
    print("Your Genderis Male")
else:
    print("Your Gender is Female")

# Print The output
iso_date = born_day.isoformat()
print(f"Your Birthday : {iso_date}")
day = born_day.strftime('%A')
print(f"Born day : {day}")