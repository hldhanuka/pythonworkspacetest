class TV:
    def __init__(self):
        self.is_on = False
        self.channel = 1
        self.volume = 5

    def turn_on(self) :
        if not self.is_on:
            self.is_on = True

            print("TV turned on.")

    def turn_off(self) :
        if self.is_on:
            self.is_on = False

            print("TV turned off.")

    def set_channel(self, channel):
        if self.is_on:
            if 1 <= channel <= 100:
                self.channel = channel

                print(f"Channerl set to {self.channel}")
            else :
                print("Invalid channer number. Enter a value between 1 to 100")
        else :
            print("TV is off. Turn on the TV to set the channel.")

    def increase_volume(self) :
        if self.is_on:
            if self.volume < 10:
                self.volume += 1

                print(f"Volume increased to level {self.volume}.")
            else :
                print("The maximum volume is reached")
        else :
            print("TV is off. Turn on the TV to set the channel.")

    def decrease_volume(self) :
        if self.is_on:
            if self.volume > 0:
                self.volume -= 1

                print(f"Volume decreased to level {self.volume}.")
            else:
                print("The minimum volume is reached")
        else :
            print("TV is off. Turn on the TV to set the channel.")

# Create a TV instance
my_tv = TV()

# Turn on the TV
my_tv.turn_on()

# Set the channel
my_tv.set_channel(5)

# Increase the volume()
my_tv.increase_volume()

# Decrease the volume
my_tv.decrease_volume()

# Trun off the TV
my_tv.turn_off()