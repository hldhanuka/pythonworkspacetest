#GCE Ordinary Level Examination Grading

#   A   75-100      Distinction
#   B   65-74.99    Very Good Pass
#   C   55-64.99    Credit Pass
#   D   40-54.99    Ordinary Pass
#   F   0-39.99     Failure
#################################################################

#Function Name  :check_grade
#Inputs         :mark,lowest,highest,grade
#Outputs        :grade,False
#Description    :returns grade if 'mark' is between low<=mark<=high

def check_grade(mark,lowest,highest,grade,text):
    if mark >= lowest  and mark <= highest:
        print(grade,' - ',text) 
        return grade
    else:
         return False

#Function Name  :check_grade_loose
#Inputs         :mark,lowest,highest,grade
#Output         :grade,False
#Description    :returns grade if 'mark' is between low<=mark<high

def check_grade_loose(mark,lowest,highest,grade,text):
    if mark >= lowest and mark < highest:
        print(grade,' - ',text)
        return grade
    else:
        return False

#Function Name  :check_grade_loose
#Inputs         :mark
#Output         :none
#Description    :prints 'Invalid mark' if 'mark' is out of range

def is_valid(mark):
    if mark < 0 or mark > 100:
        print('Invalid mark')

#################################################################

mark_val = float(input('\nEnter Marks : '))

check_grade(mark_val,75,100,'A','Distinction')
check_grade_loose(mark_val,65,75,'B','Very Good Pass')
check_grade_loose(mark_val,55,65,'C','Credit Pass')
check_grade_loose(mark_val,40,55,'D','Ordinary Pass')
check_grade_loose(mark_val,0,40,'F','Failure')
is_valid(mark_val)