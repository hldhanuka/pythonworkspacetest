import pandas as pd

# define a list of student names
students = ['Ajith', 'Kamal', 'Chamara', 'Damith', 'Ekanayake']

# define a list of subject names
subjects = ['Math', 'Science', 'English', 'History', 'Art']

# define a list of exam scores for each subject for each student
scores = [[80, 90, 85, 75, 95],
          [92, 85, 78, 82, 88],
          [75, 80, 70, 88, 92],
          [88, 92, 90, 85, 78],
          [95, 87, 92, 80, 90]]

# create a pandas DataFrame from the data
df = pd.DataFrame(scores, index=students, columns=subjects)

# define a list of grade cutoffs
cutoffs = [60, 70, 80, 90, 100]

# define a list of grades
grades = ['F', 'D', 'C', 'B', 'A']

# apply a function to calculate grades for each element in the DataFrame
def calculate_grade(score):
    for k in range(len(cutoffs)):
        if score <= cutoffs[k]:
            return grades[k]
    else:
        return grades[0]

df_grades = df.applymap(calculate_grade)

# print the grades DataFrame
print(df_grades)