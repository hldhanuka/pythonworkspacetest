class Account:
    inital_balance = 11.00

    def __init__(self, balance) -> None:
        self.__balance = balance

    def __str__(self) -> str:
        return f"Account Balance : {self.__balance}"

    @staticmethod
    def get_total_bal(bal):
        return Account.inital_balance + bal

    def get_balance(self) -> float :
        return self.__balance

account = Account(10)

print(Account.get_total_bal(account.get_balance()))