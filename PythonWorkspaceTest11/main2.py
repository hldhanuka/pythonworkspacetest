class Account:
    def __init__(self, balance) -> None:
        self.__balance = balance

    def __str__(self) -> str:
        return f"Account Balance : {self.__balance}"

    def __add__(self, other):
        if isinstance(other, Account):
            total = self.__balance + other.__balance

            return Account(total)
        else :
            raise ValueError("Unsupported opeand type for +.")

    def get_balance(self) -> float :
        return self.__balance

account1 = Account(10)
account2 = Account(15)

taccount = account1 + account2

print(taccount)