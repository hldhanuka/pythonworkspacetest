class Account:
    def __init__(self, balance) -> None:
        self.__balance = balance

    def __str__(self) -> str:
        return f"Account Balance : {self.__balance}"

    def get_balance(self) -> float :
        return self.__balance

account = Account(10)

print(account)