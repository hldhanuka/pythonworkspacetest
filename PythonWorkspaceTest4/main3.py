import in_place

def display_menu() :
    print("Simple TODO List")

    print("1 - Display the TODO List")
    print("2 - Add items to TODO List")
    print("3 - Mark item as complete")
    print("4 - Mark item as incomplete")
    print("5 - Delete item")
    print("6 - Exit")

def display_list():
    task_number = 1

    with open('todo_list.txt', 'r') as f:
        for task in f:
            task_details = task.strip()

            print(f"{task_number} | {task_details}")

            task_number += 1

        f.close()

def add_item(task):
    task_name = task[0]
    task_status = task[1]

    with open('todo_list.txt', 'a') as f:
        f.write(f"{task_name} | {task_status}\n")

        f.close()

def change_task_status(task_number, task_status) :
    task_list_number = 1

    with in_place.InPlace('todo_list.txt') as file:
        for line in file:
            if task_number == task_list_number:
                task_details = line.split(" | ")

                task_name = task_details[0]

                line = line.replace(line, f"{task_name} | {task_status} \n")

            file.write(line)

            task_list_number += 1

def delete_item(task_number) :
    task_list_number = 1

    with in_place.InPlace('todo_list.txt') as file:
        for line in file:
            if task_number != task_list_number:
                file.write(line)

            task_list_number += 1

while True :
    display_menu()

    choice = int(input("Please enter your choice : "))

    if choice == 1:
        display_list()
    elif choice == 2:
        item_name = input("Enter the task name : ")
        is_complete = False

        task = (item_name, is_complete)

        add_item(task)
    elif choice == 3:
        task_number = int(input("Enter the task number to complete: "))

        change_task_status(task_number, True)
    elif choice == 4:
        task_number = int(input("Enter the task number to in complete: "))

        change_task_status(task_number, False)
    elif choice == 5:
        task_number = int(input("Enter the task number to delete: "))

        delete_item(task_number)
    elif choice == 6:
        exit()
