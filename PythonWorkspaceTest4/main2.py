todo_list = []

def display_menu() :
    print("Simple TODO List")

    print("1 - Display the TODO List")
    print("2 - Add items to TODO List")
    print("3 - Mark item as complete")
    print("4 - Mark item as incomplete")
    print("5 - Delete item")
    print("6 - Exit")

def display_list():
    task_number = 1

    for item in todo_list:
        task_name = item[0]
        task_status = item[1]

        print(f"{task_number} | {task_name} | {task_status}")

        task_number += 1

def add_item(task):
    todo_list.append(task)

def change_task_status(task_number, task_status) :
    task = todo_list[task_number - 1]

    completed_task = (task[0], task_status)

    todo_list[task_number - 1] = completed_task

def delete_item(task_number) :
    todo_list.remove(todo_list[task_number - 1])

while True :
    display_menu()

    choice = int(input("Please enter your choice : "))

    if choice == 1:
        display_list()
    elif choice == 2:
        item_name = input("Enter the task name : ")
        is_complete = False

        task = (item_name, is_complete)

        add_item(task)
    elif choice == 3:
        task_number = int(input("Enter the task number to complete: "))

        change_task_status(task_number, True)
    elif choice == 4:
        task_number = int(input("Enter the task number to in complete: "))

        change_task_status(task_number, False)
    elif choice == 5:
        task_number = int(input("Enter the task number to delete: "))

        delete_item(task_number)
    elif choice == 6:
        exit()
