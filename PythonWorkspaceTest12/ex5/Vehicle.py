class Vehicle:
    def __init__(self, name, max_speed, mileage, color='White'):
        self.name = name
        self.max_speed = max_speed
        self.mileage = mileage
        self.color = color

    def details(self):
        return f"Color: {self.color}, Vehicle Name: {self.name},  Speed: {self.max_speed}. Mileage: {self.mileage}"

class Bus(Vehicle):
    pass

class Car(Vehicle):
    pass

bus1 = Bus('dhanuka', 10, 1)

print(bus1.details())