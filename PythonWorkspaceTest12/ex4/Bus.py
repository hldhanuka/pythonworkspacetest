from Vehicle import Vehicle

class Bus(Vehicle):
    # assign default value to capacity
    def seating_capacity(self, capacity=50):
        return super().seating_capacity(capacity)

bus1 = Bus('dhanuka', 10, 10)

print(bus1.seating_capacity())