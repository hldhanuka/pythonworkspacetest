n1 = 21
n2 = 0

try :
    result = n1 / n2

    print(f"Result : {result}")
except ZeroDivisionError:
    print(f"You have tried to divede {n1} from {n2} which is not allowed")
    print("Please retry with a non zero value")
