n1 = 21
n2 = 0

try :
    n1 = int(input("Enter n1 value :- "))
    n2 = int(input("Enter n2 value :- "))

    result = n1 / n2

    print(f"Result : {result}")
except ZeroDivisionError:
    print(f"You have tried to divede {n1} from {n2} which is not allowed")
    print("Please retry with a non zero value")
except ValueError as ex_error:
    print(ex_error)
    print("Incorrect Value")
else: 
    print("Done")
finally:
    print("Finaly Done")