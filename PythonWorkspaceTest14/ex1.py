import os
import pyautogui

os.startfile('file.txt')

pyautogui.moveTo(900, 200, duration=1)

pyautogui.click(900, 200)

pyautogui.typewrite('Hello world!')

# Saves file
pyautogui.keyDown('ctrl')
pyautogui.press('s')
pyautogui.keyUp('ctrl')

#Closes Notepad
pyautogui.keyDown('altleft')
pyautogui.press('space')
pyautogui.press('c')
pyautogui.keyUp('altleft')